// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyC2dmvoQ-HYgV_8GR-Ff-vh-LR7s-earwc",
    authDomain: "hello-orel.firebaseapp.com",
    databaseURL: "https://hello-orel.firebaseio.com",
    projectId: "hello-orel",
    storageBucket: "hello-orel.appspot.com",
    messagingSenderId: "133899426508",
    appId: "1:133899426508:web:b3065e1ffe9f72182e4069"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
